#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;
        int valor_obtenido;

    public:
        /* constructor*/
        Lista();
        
        // get valor obtenido
        int get_valor_obtenido();
        
        /* crea un nuevo nodo */
        void crear (Numero *numero);
        /* imprime la lista. */
        void imprimir ();
};
#endif
