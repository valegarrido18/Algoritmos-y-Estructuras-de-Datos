#include <iostream>
using namespace std;

/* definición de la estructura Nodo. */
#include "Ejemplo.h"

/* clases */
#include "Numero.h"
#include "Lista.h"


//Escriba un programa que cree una lista desordenada de n´umeros y luego la divida en dos listas independientes ordenadas ascendentemente, 
//una formada por los n´umeros positivos y otra por los n´umeros
//negativos.

class Ejemplo {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Ejemplo() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};

/* función principal. */
int main (void) {
    Ejemplo e = Ejemplo();
    Lista *lista = e.get_lista();

    string a;
	bool x = true;
    while(x){
		cout << "ingresar palabra ---> 1" << endl;
		cout << "salir           ---> 2" << endl;
		cin >> a;
		if (a == "1"){
			string b;			
			cout << "ingrese palabra para la lista: " << endl;
			cin >> b;
			lista->crear(new Numero(b));
			cout << "Lista:" << endl;
			lista->imprimir();
			
			
		}
		else if (a == "2"){
			x =  false;
		}
		else {
			cout << "ingrese un dato valido" << endl;
		}
	}
    return 0;
}
