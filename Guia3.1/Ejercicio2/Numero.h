#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
    private:
        string numero = "\0";

    public:
        /* constructor */
        Numero(string numero);
        
        /* método get */
        string get_numero();
};
#endif
