>> Ejercicio 3

Al ejecutar el programa aparecerá un menú del cual solo funciona el agregar postres y mostrarlos, no se pudo realizar la eliminación de postres. 


    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./main para ejecutar.

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
