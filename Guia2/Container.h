#include <iostream>
#include <stdlib.h>

using namespace std;

#ifndef CONTAINER_H
#define CONTAINER_H

class Container {
	private:
		string empresa;
		string id; 
	
		
	public: 
		/* Constructor*/
		Container(); 
		
		string get_empresa();
		void set_empresa(string empresa);
		
		string get_id();
		void set_id(string id);
		
};
#endif
