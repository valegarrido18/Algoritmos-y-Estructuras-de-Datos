#include <list>
#include <iostream>
#include <stdlib.h>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

void imprimir_datos_proteinas(list<Proteina> proteina)
{
	for (Proteina proteinaa: proteina){
		cout << "-----------------------------------------------------" << endl;
		cout << "> NOMBRE PROTEINA: " << proteinaa.get_nombre() << endl;
	    cout << "> ID:" << proteinaa.get_id() << endl;
		for (Cadena caadena: proteinaa.get_cadenas()){
			cout << "> CADENA:" << caadena.get_letra() << endl;
			for (Aminoacido aminoacido: caadena.get_aminoacidos()){
				cout << "> NOMBRE AA: " << aminoacido.get_nombre() << "  " << "> NUMERO AA:" << aminoacido.get_numero() << endl;
				for (Atomo atomo: aminoacido.get_atomo()){
					cout << "> NOMBRE ATOMO: " << atomo.get_nombre() << "  "  << "> NUMERO ATOMO:" << atomo.get_numero() << endl;
					cout << "> COORDENADAS: " << atomo.get_coordenada().get_x() << "," << atomo.get_coordenada().get_y() << ","  << atomo.get_coordenada().get_z() << endl;				
				}
			}
		}				
    }
}

void leer_datos_proteina()
{

    bool verdad = true; 
    string opcion = "\0";
	while (verdad){
		list<Proteina> proteina;	
		string string1, string2;
		cout << ">> Nombre proteina: " << endl;
		cin >> string1;
		cout << ">> ID proteina: " << endl;
		cin >> string2;

		Proteina protein = Proteina(string1, string2);
		cout << ">>Ingrese letra Cadena: " << endl;
		cin >> string1;

		Cadena cadena = Cadena(string1);	
		cout << ">> Nombre Aminoacido: " << endl;
		cin >> string1;
		cout << ">> Numero Aminoacido: " << endl;

		int aux;
		cin >> aux;

		Aminoacido amino = Aminoacido(string1, aux);
		cout << ">> Nombre Atomo " << endl;
		cin >> string1;
		cout << ">> Numero Atomo: " << endl;
		cin >> aux;					


		float coor_x = 0;
		float coor_y = 0;
		float coor_z = 0;
		
		cout << ">> Ingrese coordenada x: " << endl;
		cin >> coor_x;
		cout << ">> Ingrese coordenada y: " << endl;
		cin >> coor_y;
		cout << ">> Ingrese coordenada z: " << endl;
 		cin >> coor_z;
 		
 		Coordenada coord = Coordenada(coor_x, coor_y, coor_z);
		Atomo atomo = Atomo(string1, aux);
		atomo.set_coordenada(coord);
		
		amino.add_atomo(atomo);
		cadena.add_aminoacidos(amino);
		protein.add_cadena(cadena);	
		proteina.push_back(protein);
		imprimir_datos_proteinas(proteina);
		
		cout << "\n" << endl;
		cout << "\n" << endl;
		cout << "Desea ingresar una nueva proteina? [s/n]" << endl;	
		cin >> opcion;
		
		if (opcion == "s" || opcion == "S"){
			leer_datos_proteina();
		}
		
		if (opcion == "n" || opcion == "N"){
			verdad = false;
		}
	}
}

int main(void){
	leer_datos_proteina();    
	return 0;
}
