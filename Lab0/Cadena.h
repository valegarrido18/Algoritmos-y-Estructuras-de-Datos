#include <iostream>
#include <list>
#include "Aminoacido.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena{
    private:
		string letra = "\0";
		list<Aminoacido> aminoacidos;

    public:
        /*constructor*/
        Cadena (string letra);
        void add_aminoacidos(Aminoacido aminoacidos);
        
        /*métodos get*/
        string get_letra();
        list<Aminoacido> get_aminoacidos();
        
        /*métodos set*/
        void set_letra(string letra);
};
#endif
