#include <iostream>
#include <stdlib.h>

using namespace std;

#ifndef PROFESOR_H
#define PROFESOR_H

class Profesor {
	private:
		string nombre;
		string sexo;
		int edad;
		
	public: 
		/* Constructor*/
		Profesor ();
		
		/* get/set */ 
		string get_nombre(); 
		void set_nombre(string nombre);
		
		string get_sexo();
		void set_sexo(string sexo);
		
		int get_edad();
		void set_edad(int edad);
		
};
#endif

