#include <iostream>
using namespace std;

/* definición de la estructura Nodo. */
#include "Ejemplo.h"

/* clases */
#include "Numero.h"
#include "Lista.h"

class Ejemplo {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Ejemplo() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};

/* función principal. */
int main (void) {

	//Lista 1
    Ejemplo e = Ejemplo();
    Lista *lista = e.get_lista();
    
    //Lista 2
    Ejemplo ee = Ejemplo();
    Lista *lista2 = ee.get_lista();
    
    //Lista 3
    Ejemplo eee = Ejemplo();
    Lista *lista3 = eee.get_lista();
    
    
    string a;
	bool x = true;
    while(x){
		cout << "ingresar numero lista 1 ---> 1" << endl;
		cout << "ingresar numero lista 2 ---> 2" << endl;
		cout << "crear lista numero 3    ---> 3" << endl;
		cout << "salir                   ---> 4" << endl;
		cin >> a;
		if (a == "1"){
			int b;			
			cout << "ingrese un numero para la lista 1: " << endl;
			cin >> b;
			lista->crear(new Numero(b));
			lista3->crear(new Numero(b));
			cout << "Lista 1: " << endl;
			lista->imprimir();
			cout << endl;
		}
		else if (a == "2"){
			int b;			
			cout << "ingrese un numero para la lista 1: " << endl;
			cin >> b;
			lista2->crear(new Numero(b));
			lista3->crear(new Numero(b));
			cout << "Lista 2: " << endl;
			lista2->imprimir();
			cout << endl;
		}
		else if (a == "3"){
			// añadir numeros a la lista 3.
			cout << "Lista 1: " << endl;
			lista->imprimir();
			cout << "--------------------" << endl;
			cout << "Lista 2:" << endl;
			lista2->imprimir();
			cout << "----------------" << endl;
			cout << "Lista 3:" << endl;
			lista3->imprimir();
			cout << endl;
		}
		else if (a == "4"){
			x =  false;
		}
		else {
			cout << "ingrese un dato valido" << endl;
		}
	}
    return 0;
}
