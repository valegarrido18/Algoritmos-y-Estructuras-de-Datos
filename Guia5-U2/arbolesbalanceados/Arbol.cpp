#include <iostream>
// Definición de las funciones de la clase, es decir, la clase.
#include "Arbol.h"

using namespace std;

// Constructor vacío.
Arbol::Arbol(){

}

// Crea un nuevo nodo.
Nodo* Arbol::crear_nodo(int numero, Nodo *padre){
    Nodo *otro;

    otro = new Nodo;  

    // Valor nuevo al nodo.
    otro->dato = numero;

    // Crea subarboles izquierdo y derecho.
    otro->izq = NULL;
    otro->der = NULL;
    otro->nodo_padre = padre;
    
    return otro;
}
// Rotación de las ramas derechas.
void Arbol::rotacion_DD(Nodo *&raiz, bool bo, Nodo *nodo1){
    raiz->der = nodo1->izq;
    nodo1->izq = raiz; 

    if(nodo1->factor_equilibrio == 0){
        raiz->factor_equilibrio = 1;
        nodo1->factor_equilibrio = -1;
        bo = false;
    }
    
    else if(nodo1->factor_equilibrio == 1){
        raiz->factor_equilibrio = 0;
        nodo1->factor_equilibrio = 0;
    }

    raiz = nodo1;
}

// Rotación ramas derechas e izquierdas.
void Arbol::rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2){
    nodo2 = nodo1->izq;
    raiz->der = nodo2->izq;
    nodo2->izq = raiz; 
    nodo1->izq = nodo2->der;
    nodo2->der = nodo1;

    if(nodo2->factor_equilibrio == 1){
        raiz->factor_equilibrio = -1;
    }

    else{
        raiz->factor_equilibrio = 0;
    }

    if(nodo2->factor_equilibrio == -1){
        nodo1->factor_equilibrio = 1;
    }

    else{
        nodo1->factor_equilibrio = 0;
    }
    
    raiz = nodo2;
    nodo2->factor_equilibrio = 0;
}

void Arbol::reestructuracion_izq(bool bo, Nodo *&raiz){
    Nodo *nodo1;
    Nodo *nodo2;

    if(bo == true){
        if(raiz->factor_equilibrio == -1){
            raiz->factor_equilibrio = 0;
        }

        else if(raiz->factor_equilibrio == 0){
            raiz->factor_equilibrio = 1;
            bo = false;
        }

        else if(raiz->factor_equilibrio == 1){
            nodo1 = raiz->der;
            // Tipo de rotación a realizar.
            if(nodo1->factor_equilibrio >= 0){
               rotacion_DD(raiz, bo, nodo1);
            }

            else{
                rotacion_DI(raiz, nodo1, nodo2);
            }
        }
    }
}

// Rotación de ramas izquierdas.
void Arbol::rotacion_II(Nodo *&raiz, bool bo, Nodo *nodo1){
    raiz->izq = nodo1->der;
    nodo1->der = raiz; 
    
    if(nodo1->factor_equilibrio == 0){
        raiz->factor_equilibrio = -1;
        nodo1->factor_equilibrio = 1;
        bo = false;
    }
    
    else if(nodo1->factor_equilibrio == -1){
        raiz->factor_equilibrio = 0;
        nodo1->factor_equilibrio = 0;
    }

    // Valor nuevo a la raiz.
    raiz = nodo1;
}

// Rotación ramas izquierdas y derechas.
void Arbol::rotacion_ID(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2){
    nodo2 = nodo1->der;
    raiz->izq = nodo2->der;
    nodo2->der = raiz; 
    nodo1->der = nodo2->izq;
    nodo2->izq = nodo1;

    if(nodo2->factor_equilibrio == -1){
        raiz->factor_equilibrio = 1;
    }

    else{
        raiz->factor_equilibrio = 0;
    }

    if(nodo2->factor_equilibrio == 1){
        nodo1->factor_equilibrio = -1;
    }

    else{
        nodo1->factor_equilibrio = 0;
    }
    
    raiz = nodo2;
    nodo2->factor_equilibrio = 0;
}

void Arbol::reestructuracion_der(bool bo, Nodo *&raiz){
    Nodo *nodo1;
    Nodo *nodo2;
    

    if(bo == true){
        if(raiz->factor_equilibrio == 1){
            raiz->factor_equilibrio = 0;
        }

        else if(raiz->factor_equilibrio == 0){
            raiz->factor_equilibrio = -1;
            bo = false;
        }

        else if(raiz->factor_equilibrio == -1){
            nodo1 = raiz->izq;

            if(nodo1->factor_equilibrio <= 0){
                rotacion_II(raiz, bo, nodo1);
            }

            else{
                rotacion_ID(raiz, nodo1, nodo2);
            }
        }
    }
}

// Inserta nodo en subábol correspondiente.
void Arbol::insertar(Nodo *&raiz, int numero, bool &bo, Nodo *padre){
    int valor_raiz;

    if(raiz == NULL){
        Nodo *otro;
        otro = crear_nodo(numero, padre);
        raiz = otro;
        insertar(raiz, numero, bo, padre);
    }
    
    else{
        valor_raiz = raiz->dato;
        
        // Inserta nodos en subárbol izquierdo. 
        if(numero < valor_raiz){
            // Agrega nuevos nodos.
            insertar(raiz->izq, numero, bo, raiz);
            reestructuracion_der(bo, raiz);
            
        }
        
        // Inserta nodos en subárbol derecho.
        else if(numero > valor_raiz){
            // Agrega nuevos nodos.
            insertar(raiz->der, numero, bo, raiz);
            reestructuracion_izq(bo, raiz);
        }
       
        else{
            cout << numero << " se encuentra en el árbol";
        }
    }
    
    bo = true;
}

void Arbol::eliminar(Nodo *&raiz, bool &bo, int numero){
    Nodo *otro;
    Nodo *aux1;
    Nodo *aux2;
    bool bandera;

    if(raiz != NULL){
        // Identifica subárbol.
        if(numero < raiz->dato){
            eliminar(raiz->izq, bo, numero);
            // Reestructura elementos restantes.
            reestructuracion_izq(bo, raiz);
        }
        
        else if(numero > raiz->dato){
            eliminar(raiz->der, bo, numero);
            reestructuracion_der(bo, raiz);
        }
        
        else{
            otro = raiz;
            
            if(otro->der == NULL){
                raiz = otro->izq;
            }
            
            else{
                if(otro->izq == NULL){
                    raiz = otro->der;
                }
                
                else{ 
                    aux1 = raiz->izq;
                    bandera = false;
                   
                    // Sustitución por el nodo más a la derecha.
                    while(aux1->der != NULL){
                        aux2 = aux1;
                        aux1 = aux1->der;
                        bandera = true;
                    }

                    // Cambio del dato para la raiz nueva.
                    raiz->dato = aux1->dato;
                    otro = aux1;
                    
                    if(bandera == true){
                        aux2->der = aux1->izq;
                    }
                    
                    else{
                        raiz->izq = aux1->izq;
                        free(otro); 
                    }
                }
            }
        }
    }
    
    else{
        cout << "La información no se encuentra en el árbol";
    }
}
