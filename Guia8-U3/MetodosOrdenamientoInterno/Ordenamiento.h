#include <iostream>

using namespace std;

class Ordenamiento {
    public:
		Ordenamiento();
        void burbuja_menor(int* ,int );
        void burbuja_mayor(int* ,int );
        void insercion(int* ,int );
        void insercion_binaria(int* ,int );
        void seleccion(int* , int );
        void shell(int* , int );
        void quicksort(int* , int);
        void quicksort2(int* , int, int );
        void imprimir(int* ,int );
};
