>> Métodos ordenamiento interno. 

El parametro ver contiene 2 opciones, las cuales corresponden a las siguientes:
s = que permite ver el tiempo de cada ordenamiento en ordenar.
n = solo permite ver los tiempos.

los ordenamientos son :
Burbuja Menor
Burbuja Mayor
Insercion
Insercion Binaria
Seleccion
Shell
Quicksort
------------

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 
Graphviz


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --

Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa "tamaño del arreglo" "VER" para ejecutar. 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
