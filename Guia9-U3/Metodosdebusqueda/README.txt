>> Métodos de búsqueda - Tablas Hash 

El programa consiste tal como lo dice la guía, el ingreso de números y asimismo la búsqueda de información. Para los casos en que se encuentren colisiones habrá diferentes métodos los cuales serán prueba lineal, cuadrática, doble dirección hash y por ultimo encadenamiento, lo cual para hacer uso de ellas se deberá ingresar un parámetro al ejecutar el programa que contenga la inicial mayúscula de alguna de ellas. 


    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 
Graphviz


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, para ejecutar utilizamos ./programa junto con un parametro dependiendo del método (L, C, D y E). 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
