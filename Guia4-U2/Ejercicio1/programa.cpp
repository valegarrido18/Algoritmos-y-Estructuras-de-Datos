#include <fstream>
#include <iostream>
#include "Graficar.h"
using namespace std;

/* Para usar fork() */
#include <unistd.h>

class Grafo {
	private:
	
	public:
		Grafo(){
		}
	
		Nodo *crearNodo(int dato) {
			Nodo *q;

			q = new Nodo();
			q->izq = NULL;
			q->der = NULL;
			q->info = dato;
			return q;
		}
		
		// Ordenamiento en preOrden.
		void ordenamiento_pre(Nodo *arbol){
			if (arbol == NULL){ // Árbol vacío.
				return;
			}
			else {
				cout << arbol->info << " - ";
				ordenamiento_pre(arbol->izq);
				ordenamiento_pre(arbol->der);
			}
		}
		
		// Ordenamiento en inOrden.
		void ordenamiento_in(Nodo *arbol){
			if (arbol == NULL){
				return;
			}
			else{
				ordenamiento_in(arbol->izq);
				cout << arbol->info << " - ";
				ordenamiento_in(arbol->der);
			}
		}
		
		// Ordenamiento en postOrden.
		void ordenamiento_post(Nodo *arbol){
			if (arbol == NULL){ // si el arbol está vacio 
				return;
			}
			else {
				ordenamiento_post(arbol->izq);
				ordenamiento_post(arbol->der);
				cout << arbol->info << " - ";
			}
		}
		
		// Arega un nodo al árbol.
		void insertarNodo(Nodo *&arbol, int n){
			if (arbol == NULL){ // Árbol vacío.
				Nodo *nuevoNodo = crearNodo(n);
				arbol = nuevoNodo;
			}
			else { // Si el árbol si tiene nodos.
				int valorRaiz = arbol->info; // Se obtiene el valor de la raíz.
				if (n<valorRaiz){ // Si el elemento es menor que la raíz, se inserta en izq.
					insertarNodo(arbol->izq,n);
				}
				else if (n>valorRaiz){ 
					insertarNodo(arbol->der,n);
				}
				
				else{ 
					cout << "El valor ya existe!" << endl;
				} 
			}
		}
};
 
// Función principal.
int main(void) {
	// Creación del arbol.
	Nodo *raiz = NULL;
	int opcion;
	bool aux=true;
	int dato;
	
	// Se crea objeto de tipo grafo.
	Grafo *g = new Grafo();
	
	// Menú de opciones.
	while (aux){
		cout << endl;
		cout << "Insertar número        [1]" << endl;
		cout << "Graficar árbol         [2]" << endl;
		cout << "mostrar árbol ordenado [3]" << endl;
		cout << endl;
		cout << "Presione cualquier letra para salir " << endl;
		cin >> opcion;
		
		system ("clear"); 
		
		if (opcion == 1){
			cout << "valor: " << endl;
			cin >> dato;
			g->insertarNodo(raiz,dato);
		}
		else if (opcion == 2){
			// Se crea un objeto de la clase Graficar.
			Graficar *y = new Graficar(raiz);
			// Se llama la funcion principal de la clase.
			y->crearGraficar();
		}
		else if (opcion == 3){
			cout << "Datos ordenados en pre orden" << endl;
			g->ordenamiento_pre(raiz);
			cout << endl;
			cout << "Datos ordenados en in orden" << endl;
			g->ordenamiento_in(raiz);
			cout << endl;
			cout << "Datos ordenados en post orden" << endl;
			g->ordenamiento_post(raiz);
			cout << endl;
		}
		else {
			aux=false;
		}
	}
    return 0;
}
