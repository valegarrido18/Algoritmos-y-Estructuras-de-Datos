>> Ejercicio 1

Al ejecutar el programa se mostrará un menú que idicará las opciones de ingresar valores tipo int al árbol siempre y cuando estos no se repitan ya que los numeros se deben ingresar de manera única, mostrar el arbol graficado y mostrar el árbol de manera pre-orden, in-orden y post-orden. Para finalizar el programa se cerrará mientras el usuario presione cualquier letra. 

*El programa no elimina ni edita los elementos del árbol. 

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./grafo para ejecutar.

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
