#include <iostream>
#include <fstream>

using namespace std;

// Archivo .h con todas las cabeceras.

typedef struct _Nodo {
	int info;
	struct _Nodo *izq;
	struct _Nodo *der;
} Nodo;

class Graficar {
	private:
		Nodo *arbol = NULL;
		
	public:
		// Constructor.
		Graficar(Nodo *arbol);
		
		//Métodos de la clase Grafo.
		void crearGraficar();
		void recorrerArbol (Nodo *, ofstream &);        
};
