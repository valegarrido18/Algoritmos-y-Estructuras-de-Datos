>> Ejercicio 3 <<

EL programa consiste en poder agregar los datos de clientes para asi determinar quienes de ellos son morosos o no, se pide primero la cantidad de clientes que se desean agregar, luego se pediran los datos correspondientes a nombr, telefono y saldo, para aquellos que tengan saldo igual a 0 corresponderan a que no son morosos, pero para aquellos que su saldo sea distinto a 0 significará que tendrán deuda por lo tanto corresponden a morosos. 

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
