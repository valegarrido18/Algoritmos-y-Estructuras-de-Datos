>> PILAS  <<

Al ejecutar el programa se mostrará por pantalla un menú sencillo el cual indicará 4 opciones de 1 a 0 en la primera se deberán ingresar 4 valores que desee el usuario, la segunda opcion permite remover(eliminar) uno de los valores que se hayan ingresado, mientras que, la tercera opción nos permitirá ver la pila, es decir, ver todos los numeros ingresados desde el primer ingresado al ultimo quedando de los primeros, tal como lo es una pila. Para finalizar se encontrará la opción salir que cerrará el programa.


    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa agregando un parámetro que dará la dimensión del arreglo. 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
