#include <iostream>
#include <stdlib.h>
#include "Pila.h"
using namespace std;


int main(){
	string opciones;
	Pila pila = Pila();
	bool u = true;
	while (u){
		cout << "Agregar  [1]" << endl; 
		cout << "Remover  [2]" << endl;
		cout << "Ver pila [3]" << endl;
		cout << "Salir    [0]" << endl;
		cout << "----------------" << endl;
		cin >> opciones;
		if (opciones == "1" ){	
			pila.Push();
		}
		
		if (opciones == "2" ){
			pila.Pop();
		}
		
		if (opciones == "3" ){
			pila.ver_datos();
		}
		
		if (opciones == "0" ){
			exit(0);
		
		}
	}
	return 0;
} 
